#include <DNSServer.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include "ESPAsyncWebServer.h"
#include <FastLED.h>
#include <SPIFFS.h>

IPAddress apIP(8,8,4,4);

#define BOT_NUM_LEDS 67
#define SUB_NUM_LEDS 17
#define BOT_DATA_PIN 13
#define SUB_DATA_PIN 12
CRGB bot_leds[BOT_NUM_LEDS];
CRGB sub_leds[SUB_NUM_LEDS];
#define MAX_CURRENT_MILLIAMPS 3000

DNSServer dnsServer;
AsyncWebServer server(80);

bool bot_change = false;
uint8_t bot_bright = 20;
CRGB bot_color = CRGB::Blue;

bool sub_change = false;
uint8_t sub_bright = 20;
CRGB sub_color = CRGB::Blue;

static String template_processor(const String& val)
{
	char buf[6];

	if(val == "SUBBRIGHT"){
		return String(sub_bright);
	}
	if(val == "SUBCOLOR"){
		sprintf(buf,"%06x",((unsigned int)sub_color.r << 16) | ((unsigned int)sub_color.g << 8 ) | (unsigned int)sub_color.b);
		return String(buf);
	}
	if(val == "BOTBRIGHT"){
		return String(bot_bright);
	}
	if(val == "BOTCOLOR"){
		sprintf(buf,"%06x",((unsigned int)bot_color.r << 16) | ((unsigned int)bot_color.g << 8 ) | (unsigned int)bot_color.b);
		return String(buf);
	}
	return String();
}

class CaptiveRequestHandler : public AsyncWebHandler {
public:
	CaptiveRequestHandler() {}
	virtual ~CaptiveRequestHandler() {}

	bool canHandle(AsyncWebServerRequest *request){
		//request->addInterestingHeader("ANY");
		return true;
	}

	void handleRequest(AsyncWebServerRequest *request) {
		request->send(SPIFFS, "/redirect.html","text/html", false);
	}
};

void setupServer(){
	server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
		request->send(SPIFFS, "/portal.html","text/html", false, template_processor);
		Serial.println("Client Connected");
	});

	server.on("/setsub", HTTP_GET, [] (AsyncWebServerRequest *request) {
		String paramVal;
		char colorbuf[7];

		if (request->hasParam("sub-bright")) {
			sub_bright = request->getParam("sub-bright")->value().toInt();
			sub_change = true;
		}

		if (request->hasParam("sub-color")) {
			paramVal = request->getParam("sub-color")->value();
			Serial.print("Got color: ");
			Serial.println(paramVal);
			if (paramVal.startsWith("#")) {
				paramVal.substring(1).toCharArray(colorbuf, 7);
				sub_color = strtol(colorbuf, 0, 16);
				sub_change = true;
			}
		}

			request->redirect("/");
	});

	server.on("/setbot", HTTP_GET, [] (AsyncWebServerRequest *request) {
		String paramVal;
		char colorbuf[7];

		if (request->hasParam("bot-bright")) {
			bot_bright = request->getParam("bot-bright")->value().toInt();
			bot_change = true;
		}

		if (request->hasParam("bot-color")) {
			paramVal = request->getParam("bot-color")->value();
			Serial.print("Got color: ");
			Serial.println(paramVal);
			if (paramVal.startsWith("#")) {
				paramVal.substring(1).toCharArray(colorbuf, 7);
				bot_color = strtol(colorbuf, 0, 16);
				bot_change = true;
			}
		}

			request->redirect("/");
	});
}

void updateLeds(CRGB leds[], int num, CRGB color, uint8_t brightness) {
	for (int i = 0; i < num; i++) {
		leds[i] = color;
	}
	FastLED.setBrightness(brightness);
	FastLED.show();
}

void startup() {
	updateLeds(sub_leds, SUB_NUM_LEDS, CRGB::Black, 255);
	updateLeds(bot_leds, BOT_NUM_LEDS, CRGB::Black, 255);
	for(int u=0; u<BOT_NUM_LEDS/2; u++) {
		bot_leds[u] = CRGB::Blue;
		bot_leds[BOT_NUM_LEDS-u-1] = CRGB::Blue;
		if (u > 3 && u < SUB_NUM_LEDS+4)
			sub_leds[u-4] = CRGB::Blue;
		delay(50);
		FastLED.show();
	}
	for(int u=0; u<BOT_NUM_LEDS/2; u++) {
		bot_leds[u] = CRGB::Black;
		bot_leds[BOT_NUM_LEDS-u-1] = CRGB::Black;
		if (u > 3 && u < SUB_NUM_LEDS+4)
			sub_leds[u-4] = CRGB::Black;
		delay(50);
		FastLED.show();
	}
}

void setup(){
	//your other setup stuff...
	Serial.begin(115200);
	Serial.println();
	if(!SPIFFS.begin(true)){
		Serial.println("An Error has occurred while mounting SPIFFS");
		return;
	}
	Serial.println("Setting up AP Mode");
	WiFi.mode(WIFI_AP);
	WiFi.softAP("DieBass LEDs", "dicketitten123");
	WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
	Serial.print("AP IP address: ");
	Serial.println(WiFi.softAPIP());
	Serial.println("Setting up Async WebServer");
	setupServer();
	Serial.println("Starting DNS Server");
	dnsServer.start(53, "*", WiFi.softAPIP());
	server.addHandler(new CaptiveRequestHandler()).setFilter(ON_AP_FILTER);//only when requested from AP
	//more handlers...
	server.begin();
	Serial.println("Setting up FastLED");
	FastLED.addLeds<NEOPIXEL, BOT_DATA_PIN>(bot_leds, BOT_NUM_LEDS);
	FastLED.addLeds<NEOPIXEL, SUB_DATA_PIN>(sub_leds, SUB_NUM_LEDS);
	FastLED.setMaxPowerInVoltsAndMilliamps(5, MAX_CURRENT_MILLIAMPS);
	startup();
	Serial.println("All Done!");
}

void loop(){
	dnsServer.processNextRequest();
	if(sub_change){
		updateLeds(sub_leds, SUB_NUM_LEDS, sub_color, sub_bright);
		sub_change = false;
	}
	if(bot_change){
		updateLeds(bot_leds, BOT_NUM_LEDS, bot_color, bot_bright);
		bot_change = false;
	}
}
